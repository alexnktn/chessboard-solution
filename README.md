# chessboard-solution
This is a solution to the problem of chess knight finding a shortest amount of steps to go from point A to point B on a chessboard.

Prerequisites:

1) docker-compose. Version 1.20.1 or higher
2) Docker. Version 18.03.0-ce or higher
3) Java 8
4) Maven 3.3.9 or higher

Run inside docker machine or on Docker engine.

Build project first:

    mvn clean install

Run docker-compose command:

    docker-compose up -d

Navigate to you browser:

    httpd://DOCKER_MACHINE_IP/


## Things to improve:

- Add Unit testing
- Add more JavaDoc
- Polish UI