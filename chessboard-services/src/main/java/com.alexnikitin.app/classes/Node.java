package com.alexnikitin.app.classes;

import java.util.ArrayList;
import java.util.List;

/**
 * Node class that maintains position and distance from starting point to itself
 */
public class Node
{
    // (x, y) represents chess board coordinates
    // dist represent its minimum distance from the source
    private int x, y, dist;
    // Store all previous nodes to this point
    private List<Node> previousNodeList = new ArrayList<>();

    public Node(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Node(int x, int y, int dist) {
        this.x = x;
        this.y = y;
        this.dist = dist;
    }

    /**
     * As we are using class object as a key in a HashMap
     * we need to implement hashCode() and equals()
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node node = (Node) o;

        if (x != node.x) return false;
        if (y != node.y) return false;
        return dist == node.dist;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        result = 31 * result + dist;
        return result;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getDist() {
        return dist;
    }

    public void setDist(int dist) {
        this.dist = dist;
    }

    public List<Node> getPreviousNodeList() {
        return previousNodeList;
    }

    public void setPreviousNodeList(List<Node> previousNodeList) {
        this.previousNodeList = previousNodeList;
    }
};