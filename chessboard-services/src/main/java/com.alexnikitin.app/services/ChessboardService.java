package com.alexnikitin.app.services;

import com.alexnikitin.app.classes.Node;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("chessboardService")
public class ChessboardService {
    // arrays for possible knight movements
    private static int row[] = { 2, 2, -2, -2, 1, 1, -1, -1 };
    private static int col[] = { -1, 1, 1, -1, 2, -2, 2, -2 };
    private int dimensionBoard = 8;
    Map<String, Integer> chessboardMapping = new HashMap<String, Integer>() {{
        put("a", 1);
        put("b", 2);
        put("c", 3);
        put("d", 4);
        put("e", 5);
        put("f", 6);
        put("g", 7);
        put("h", 8);
    }};

    public String getAppInfo() {
        return "Chess knight's moves solution.";
    }

    /**
     * Return integer representation of chessboard for X.
     * @param a Alphabetical value from A to H
     * @return
     */
    public int getIntPositionOfColumn(String a){
        return chessboardMapping.get(a.toLowerCase());
    }

    /**
     * Return character representation of chessboard for X.
     * @param a Value between 1 and dimensionBoard
     * @return
     */
    public String getCharPositionOfColumn(int a){
        for (Map.Entry<String, Integer> entry : chessboardMapping.entrySet()) {
            if (Objects.equals(a, entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * Check if (x, y) is valid chess board coordinates
     * Note that a knight cannot go out of the chessboard
     */
    private boolean valid(int x, int y, int dimensionBoard)
    {
        if (x < 0 || y < 0 || x >= dimensionBoard || y >= dimensionBoard)
            return false;
        return true;
    }

    /**
     * Find minimum number of steps taken by the knight
     * from source to reach destination using BFS algorithm
     */
    public List<Node> findSteps(Node src, Node dest)
    {
        // map to check if matrix cell is visited before or not
        Map<Node, Boolean> visited = new HashMap<>();

        // create a queue and enqueue first node
        Queue<Node> q = new ArrayDeque<>();
        q.add(src);
        int steps = 0;
        // run till queue is not empty
        while (!q.isEmpty())
        {
            // pop front node from queue and process it
            Node node = q.poll();

            int x = node.getX();
            int y = node.getY();
            int dist = node.getDist();

            // if destination is reached, return distance
            if (x == dest.getX() && y == dest.getY()) {
                node.getPreviousNodeList().add(dest);
                return node.getPreviousNodeList();
            }

            // Skip if location is visited before
            if (visited.get(node) == null)
            {
                // mark current node as visited
                visited.put(node, true);

                // check for all 8 possible movements for a knight
                // and enqueue each valid movement into the queue
                for (int i = 0; i < 8; ++i)
                {
                    // Get the new valid position of Knight from current
                    // position on chessboard and enqueue it in the
                    // queue with +1 distance
                    int x1 = x + row[i];
                    int y1 = y + col[i];

                    if (valid(x1, y1, this.dimensionBoard)) {
                        Node nextNode = new Node(x1, y1, dist + 1);
                        nextNode.getPreviousNodeList().addAll(node.getPreviousNodeList());
                        nextNode.getPreviousNodeList().add(node);
                        q.add(nextNode);

                    }
                }
            }
        }
        return null;
    }
}
