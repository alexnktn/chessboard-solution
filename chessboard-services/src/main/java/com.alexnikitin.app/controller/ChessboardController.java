package com.alexnikitin.app.controller;

import com.alexnikitin.app.classes.Node;
import com.alexnikitin.app.services.ChessboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Main chessboard controller
 */
@RestController
public class ChessboardController {

    @Autowired
    private ChessboardService chessboardService;

    @RequestMapping(value = "/info", method = RequestMethod.GET, produces = "application/json")
    public String getInfo() {
        return "Running app: " + chessboardService.getAppInfo();
    }

    @RequestMapping(value = "/findPath/start/{start}/finish/{end}", method = RequestMethod.GET,
            produces="application/json; charset=UTF-8")
    public Map<String, String> findPath(@PathVariable String start, @PathVariable String end) {
        // source coordinates
        Node startPoint = new Node(chessboardService.getIntPositionOfColumn(start.substring(0,1)) - 1, Integer.parseInt(start.substring(1,2)) - 1);
        // destination coordinates
        Node finishPoint = new Node(chessboardService.getIntPositionOfColumn(end.substring(0,1)) - 1, Integer.parseInt(end.substring(1,2)) - 1);

        List<Node> allNodeList = chessboardService.findSteps(startPoint, finishPoint);
        Map<String, String> result = new HashMap<>();
        int count = 0;
        for (Node node: allNodeList){
            String key = chessboardService.getCharPositionOfColumn(node.getX());
            result.put(Integer.toString(count), chessboardService.getCharPositionOfColumn(node.getX() + 1) + (node.getY() + 1));
            count++;
        }
        return result;
    }
}
