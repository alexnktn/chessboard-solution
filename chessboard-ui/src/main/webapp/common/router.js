define(['backbone', 'header/header', 'solution/chessboard'],
        function(Backbone, header, chessboard){
    var Router = Backbone.Router.extend({
        routes: {
            "*path" : "loadMain"

        },
        initialize: function(){
            var pushState = history.pushState;
            history.pushState = function(state, title, path) {
            		if(state.trigger){
            			this.router.navigate(path, state);
            		}else{
            			this.router.navigate(path, {trigger: true});
            		}
                return pushState.apply(history, arguments);
            }.bind({router:this});
        },
       
        execute: function(callback, args){
            if (callback) {
                callback.apply(this, args);
            }
        },

        loadMain : function(){
            var headerView = header.View;
            headerView.render();
            $('#header-content').append(headerView.$el);

            var chessboardView = new chessboard.View({model: new chessboard.Model()});
            chessboardView.render();
            $('#main-content').append(chessboardView.$el);
        }

    });
    return new Router();
});