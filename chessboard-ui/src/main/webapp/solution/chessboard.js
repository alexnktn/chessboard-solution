define(["backbone","handlebars", "text!solution/chessboard.hbs", "solution/services"],
		function(BB, HBS, template, services){
    var chessboardModel = BB.Model.extend({
        startPoint: null,
        endPoint: null
    });

    var chessboardView = BB.View.extend({
		initialize : function(opts){
			this.template = HBS.compile(template);
		},
		events : {
			"click .square":   "registerPosition"
        },
        displayChessboard: function () {
            var length = 8;
		    var chessboard = "";
		    var columns = ["a","b","c","d","e","f","g","h"]
            var cell = 0;
            for (var i=0; i<length; i++) {
                var col = "";
                for (var j=0; j<length; j++) {
                    col += "<td class='square' id='" + columns[j] + (length - i) + "'></td>";
                    cell++;
                }
                chessboard += "<tr>"+col+"</tr>";
            }
            $('.chessboard-main', this.$el).html(chessboard);

        },
        registerPosition: function (event) {
		    if (this.model.endPoint && this.model.startPoint) {
                this.model.endPoint = this.model.startPoint = null;
		        this.render();
		        return;
            }
            var position = event.target.id;
            $(event.target).addClass('highlight');

            if (this.model.startPoint && !this.model.endPoint) {
                this.model.endPoint = position;
            } else {
                this.model.endPoint = null;
                this.model.startPoint = position;
            }
            if (this.model.endPoint && this.model.startPoint) {
                services.findPath(this.model, function (result) {
                    this.showResults(result);
                }.bind(this));
            }
            $('.chessboard-position', this.$el).append("<div>At position " + position + "</div>");
        },
        showResults: function (result) {
            for (var key in result) {
                $('#' + result[key], this.$el).html(key);
            }
        },
        render : function(){
            this.$el.html(this.template({}));
            this.displayChessboard()
		}
	});

	return {
		View : chessboardView,
        Model: chessboardModel
	};
});