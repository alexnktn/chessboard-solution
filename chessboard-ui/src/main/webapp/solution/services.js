define(["util/notification"],
		function(notification){
    var services = {
        init: function () {}
    };
    services.findPath = function (object, callback) {
        $.ajax({
            url: window.location.origin + '/services/findPath/start/' + object.startPoint + '/finish/' + object.endPoint,
            type: 'GET',
            contentType: 'application/json',
            success: function(response){
                notification.showSuccessMessage("Found a solution!");
                callback(response);
            }.bind(object),
            error: function(response){
                notification.showFailureMessage();
            }
        });
    }.bind(services);

	return services;
});